from control import TransferFunction as tf,feedback, zero
from numpy import log, pi, prod, arange
import matplotlib.pyplot as plt
import csv
from scipy.signal import lti, step
from math import e


def Grafico(ax,ay,titulo):
	#Faz um degrau só para exibir gráficamente
	Tstep = [-ax.max()/50,0,1e-15,ax.max()]
	Ystep = [0,0,1,1]


	#prepara algumas informações da legenda
	Pico = max(ay)
	#print("Pico = ",Pico)
	Final = 1
	#print("Estabiliza em  = ",ay[-1])
	SobreSinal_c = 100*(Pico-Final)/Final
	#print("SobreSinal_c = ",SobreSinal_c,"%")
	Erro = (1-ay[-1])*100
	#print("Erro = ",Erro,"%")

	subtitulo = '\nStep Response - (K='
	subtitulo += str(round(K,3))
	subtitulo += ' a='
	subtitulo += str(round(a,3))
	subtitulo += ')'

	#Monta o Gráfico
	plt.figure()
	plt.title(titulo+subtitulo)                  # Titulo
	plt.plot(ax,ay,label='Malha Fechada: Erro = '+str(round(Erro,3))+'%' )                # Titulo
	plt.plot(Tstep,Ystep,label='Step')
	plt.plot([ax[0],ax[-1]],[Pico,Pico],label='Sobressinal: MP = '+str(round(SobreSinal_c,3))+'%' )
	plt.ylabel('gain')        # Plota o label ay
	plt.xlabel('time [s]')            # Plota o label x
	plt.grid(which='both', axis='both')     # Gride para frequências intermediárias
	plt.grid(True)                          # Mostra o Grid
	plt.margins(0, 0.1)                     # Deixa uma margem
	plt.legend()
	plt.savefig(titulo+'.png')
	plt.clf()

#	plt.savefig('gif\\'+titulo+'.png')

#Pontos de resolução
dots = 5000

#Parâmetros do FT experimental
wn1 = 40.21
wn2 = 207.27
k = 6417.43
TP = 0.013
MP = 0.13

#Método de Alocação de Pólos
b0 = k
a1 = wn1+wn2
a0 = wn1*wn2
g = tf([b0],[1,a1,a0])

K = arange(0.1,2,0.1)
a = arange(350,500,1)
solution=[]
k=0
maxM = 0

titulos = 'Metodo da forca Bruta'


for i in range(len(K)):
	for j in range(len(a)):
		controler = K[i]*tf([1,2*a[j],a[j]**2], [1,0]); # controlador
		sys = feedback(controler*g) # Fundação de transferência em malha fechada
		sys2 = sys.returnScipySignalLTI()[0][0]
		t,y = step(sys2,N = dots)
		m = max(y)
		if (m < (1+MP)): 
			k = k+1;
			solution.append([K[i],a[j],m])
			if(maxM < m):
				maxM=m
				best=[K[i],a[j],m]

K = best[0]
#print(K)
a = best[1]
#print(a)

controler = K*tf([1,2*a,a**2], [1,0]); # controlador
sys = feedback(controler*g) # Fundação de transferência em malha fechada
sys2 = sys.returnScipySignalLTI()[0][0]
t,y = step(sys2,N = dots)


titulos = 'Metodo da forca Bruta'
Grafico(t,y,titulos)


for i in range(2295,len(solution)):
	K = solution[i][0]
	#print(K)
	a = solution[i][1]
	#print(a)

	controler = K*tf([1,2*a,a**2], [1,0]); # controlador
	sys = feedback(controler*g) # Fundação de transferência em malha fechada
	sys2 = sys.returnScipySignalLTI()[0][0]
	t,y = step(sys2,N = dots)

	Grafico(t,y,'gif\\'+str(i)+' '+titulos)



'''
#Faz um degrau só para exibir gráficamente
Tstep = [-t.max()/50,0,1e-15,t.max()]
Ystep = [0,0,1,1]


#prepara algumas informações da legenda
Pico = max(y)
#print("Pico = ",Pico)
Final = 1
#print("Estabiliza em  = ",y[-1])
SobreSinal_c = 100*(Pico-Final)/Final
#print("SobreSinal_c = ",SobreSinal_c,"%")
Erro = (1-y[-1])*100
#print("Erro = ",Erro,"%")

titulo = 'Método da força Bruta'
subtitulo = '\nStep Response - (K='
subtitulo += str(round(K,3))
subtitulo += ' a='
subtitulo += str(round(a,3))
subtitulo += ')'

#Monta o Gráfico
plt.title(titulo+subtitulo)                  # Titulo
plt.plot(t,y,label='Malha Fechada: Erro = '+str(round(Erro,3))+'%' )                # Titulo
plt.plot(Tstep,Ystep,label='Step')
plt.plot([t[0],t[-1]],[Pico,Pico],label='Sobressinal: MP = '+str(round(SobreSinal_c,3))+'%' )
plt.ylabel('gain')        # Plota o label y
plt.xlabel('time [s]')            # Plota o label x
plt.grid(which='both', axis='both')     # Gride para frequências intermediárias
plt.grid(True)                          # Mostra o Grid
plt.margins(0, 0.1)                     # Deixa uma margem
plt.legend()
plt.savefig(titulo+'.png')'''

#plt.show()