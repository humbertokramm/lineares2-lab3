clear;
clc;
% Valores de �K� e �a� para teste
%K = [2.0 2.2 2.4 2.6 2.8 3.0];
K = 0.1:0.1:2;
%a = [0.5 0.7 0.9 1.1 1.3 1.5];
a = 350:1:500;
% Avalia a resposta ao degrau unit�rio em malha fechada em cada combina��o
% de 'K' e 'a' que far� o m�ximo sobressinal ser menor que 10%
t = 0:0.00001:0.1;
%g = tf([1.2],[0.36 1.86 2.5 1]);
g = zpk([],[-40.21 -207.3], 6417.4)
k = 0;
for i = 1:size(K,2);
    for j = 1:size(a,2);
        gc = tf(K(i)*[1 2*a(j) a(j)^2], [1 0]); % controlador
        G = gc*g/(1 + gc*g); % Funda��o de transfer�ncia em malha fechada
        y = step(G,t);
        m = max(y);
        if m < 1.13 
            k = k+1;
            solution(k,:) = [K(i) a(j) m];
        end
    end
end

solution % Imprime a tabela de solu��o

sortsolution = sortrows(solution,3) % Imprime a tabela de solu��o ordenada
% pela coluna 3

% Gera o gr�fico da resposta com o maior sobressinal que � menor que 10%
K = sortsolution(k,1)
a = sortsolution(k,2)
gc = tf(K*[1 2*a a^2], [1 0]);
G = gc*g/(1 + gc*g);
step(G,t)
grid % Veja Figura 8� 20
% Se voc� quiser exibir a resposta com o menor sobressinal que � maior do
% que 0%, digite os seguintes valores de �K� e �a�
%K = sortsolution(11,1)
%a = sortsolution(11,2)

gc = tf(K*[1 2*a a^2], [1 0]);
G = gc*g/(1 + gc*g);
step(G,t)
grid % Veja Figura 8� 21