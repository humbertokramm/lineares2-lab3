clc
sys = zpk([],[-40.21 -207.27],6417.43)
%Ts= 1
%sys_z = c2d(sys,Ts,'ZoH')


%slide 11, da aula 15
%dado um controlador...
s_t = 1/20000
c = zpk([-275.29],[-525.8],[35.68])
bode(c)
grid on
c_z = c2d(c,s_t,'ZoH')
zer_z = c_z.Z{(1)}
pol_z = c_z.P{(1)}
k_z = c_z.K

% PID
% 0.3(s+351)^2 / s, Kp = 702, Td = 1/702, Ti = 1/175.5
Ts = 1/10000
Kp = 702 * 0.3
Td = 1/702
Ti = 1/175.5
s = zpk('s')
Cpid = Kp*(1+(1/(Ti*s))+Td*s)
q0 = Kp*(1+(Td/Ts))
q1 = -Kp*(1+(2*(Td/Ts))-(Ts/Ti))
q2 = Kp*(Td/Ts)
bode(Cpid)
grid on


