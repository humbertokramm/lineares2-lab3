from control import TransferFunction as tf
import matplotlib.pyplot as plt
from scipy.signal import lti, step

#Pontos de resolução
dots = 5000

#sistema
k = 1
wn = [0,1,3]
a = 1
b = sum(wn)
c = wn[0]*wn[1]+wn[0]*wn[2]+wn[1]*wn[2]
d = wn[0]*wn[1]*wn[2]

#Monta a FT
G = tf([k],[a, b, c,d])
print("G(s)= ",G)
sysMA = G.returnScipySignalLTI()[0][0]
tMA,yMA = step(sysMA,N = dots)


#Faz um degrau só para exibir gráficamente
Tstep = [-0.5,0,1e-15,tMA.max()]
Ystep = [0,0,1,1]


#Monta o Gráfico
titulo = 'Método 1 Ziegler-Nichols'
plt.title(titulo)                  # Titulo
plt.plot(tMA,yMA,label='Malha Aberta')                      # Titulo

plt.plot(Tstep,Ystep,label='Step')
plt.ylabel('gain')        # Plota o label y
plt.xlabel('time [s]')            # Plota o label x
plt.grid(True)                          # Mostra o Grid
plt.margins(0, 0.1)                     # Deixa uma margem
plt.legend()
plt.savefig(titulo+'.png')

plt.show()