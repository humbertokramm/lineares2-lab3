clc
%Par�metros do exerc�cio de aula
wn1 = 4
wn2 = 11
k = 16
TP = 0.9
MP = 0.13

%Par�metros do FT experimental
wn1 = 40.21
wn2 = 207.27
k = 6417.43
TP = 0.013
MP = 0.13

%M�todo de Aloca��o de P�los
b0 = k
a1 = wn1+wn2
a0 = wn1*wn2
g = zpk([],[-wn1 -wn2],b0)

ksi = ( ((log(MP)/pi)^2) / (1+(log(MP)/pi)^2) )^(1/2)
wn = pi/(TP*sqrt(1-ksi^2))

Z1 = 5*wn

Q = tf([1 Z1],1)*tf([1 2*ksi*wn wn^2],1)
num = tfdata(Q);
q2 = num{1,1}(2)
q1 = num{1,1}(3)
q0 = num{1,1}(4)

C0 = q0/b0
C1 = (q1-a0)/b0
C2 = (q2-a1)/b0

Kp = C1
Td = C2/Kp
Ti = Kp/C0

I = zpk([],[0],1/Ti)
D = zpk([0],[],Td)

t=0:0.0001:0.05;

figure
%PID
PID_ = Kp*(1+I+D)
Gmf = feedback(PID_*g,1)
step(Gmf,t)
hold
grid on



Ts_PID = 1e-4
q0 = Kp*(1+Td/Ts_PID)
q1 = -Kp*(1+2*Td/Ts_PID-Ts_PID/Ti)
q2 = Kp*Td/Ts_PID
