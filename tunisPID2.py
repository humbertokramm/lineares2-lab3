from control import TransferFunction as tf,feedback
from numpy import pi
import matplotlib.pyplot as plt
from scipy.signal import lti, step

#Pontos de resolução
dots = 5000

Kc = 12 #calculado através do método da instabilidade marginal
Pcr = 2*pi/(3**(1/2)) #w do ganho crítico
#Parâmetros do controlador
Kp = 0.6*Kc
Ti = 0.5*Pcr
Td = 0.125*Pcr

#sistema
k = 1
wn = [0,1,3]
a = 1
b = sum(wn)
c = wn[0]*wn[1]+wn[0]*wn[2]+wn[1]*wn[2]
d = wn[0]*wn[1]*wn[2]

#Monta a FT
G = tf([k],[a, b, c,d])
print("G(s)= ",G)
sysMA = G.returnScipySignalLTI()[0][0]
tMA,yMA = step(sysMA,N = dots)

#Monta o sistema de controle
print("Kp= ",Kp)
print("Ti= ",Ti)
print("Td= ",Td)
controler = Kp*(1+tf([1/Ti],[1,0])+tf([Td,0],[1]))
print("controler= ",controler)


#Realimenta o sistema com o controlador aplicado
print("controler*G= ",controler*G)
sys = controler*G
sys = feedback(controler*G)
print("sys= ",sys)
#Aplica o degrau
sys2 = sys.returnScipySignalLTI()[0][0]
t,y = step(sys2,N = dots)

#Faz um degrau só para exibir gráficamente
Tstep = [-0.5,0,1e-15,t.max()]
Ystep = [0,0,1,1]


#Pega o segundo sobre sinal
overshoot = 0
for j in range(len(y)):
	if(overshoot == 0):
		if(y[j]>1):
			overshoot = 1
	elif(overshoot == 1):
		if(y[j]<1):
			overshoot = 2
	elif(overshoot == 2):
		if(y[j]>1):
			break

#prepara algumas informações da legenda
Pico = max(y[j:])
print("Pico = ",Pico)
Final = 1
print("Estabiliza em  = ",y[-1])
SobreSinal_c = 100*(Pico-Final)/Final
print("SobreSinal_c = ",SobreSinal_c,"%")
Erro = (1-y[-1])*100
print("Erro = ",Erro,"%")


titulo = 'Método 2 Ziegler-Nichols'
subtitulo = '\nStep Response - (Kp='
subtitulo += str(round(Kp,3))
subtitulo += ' Ti='
subtitulo += str(round(Ti,3))
subtitulo += ' Td='
subtitulo += str(round(Td,3))
subtitulo += ')'

#Monta o Gráfico
plt.title(titulo+subtitulo)                  # Titulo
plt.plot(t,y,label='Malha Fechada: Erro = '+str(round(Erro,3))+'%' )                # Titulo
plt.plot(Tstep,Ystep,label='Step')
plt.plot([t[0],t[-1]],[Pico,Pico],label='Sobressinal: MP = '+str(round(SobreSinal_c,3))+'%' )
plt.ylabel('gain')        # Plota o label y
plt.xlabel('time [s]')            # Plota o label x
plt.grid(which='both', axis='both')     # Gride para frequências intermediárias
plt.grid(True)                          # Mostra o Grid
plt.margins(0, 0.1)                     # Deixa uma margem
plt.legend()
plt.savefig(titulo+'.png')

plt.show()