clc

PoloFT1 = -40.21
PoloFT2 = -207.27
K = 6417.43
Kc = 18.68


GanhoC = 1.91 
ZeroC = -275.21
PoloC = -525.77

GanhoC = GanhoC*Kc

G = zpk([],[PoloFT1,PoloFT2],K)

gC = zpk([ZeroC],[PoloC],GanhoC)

Ts = 1/abs(PoloC*10)
SR = Ts
gC_z=c2d(gC,Ts,'zoh')
%gC_z=c2d(gC,Ts,'tustin')

ZeroD = gC_z.Z{(1)}
PoloD = gC_z.P{(1)}
GanhoD = gC_z.K


%%%%%%%%%%%%
%   PID    %
%%%%%%%%%%%%
Kp = 1086.326
Ti = 1.238
Td = 0.873
Kp = 210.6000
Td = 0.0014
Ti =  0.0057

Ts_PID = 1e-4
q0 = Kp*(1+Td/Ts_PID)
q1 = -Kp*(1+2*Td/Ts_PID-Ts_PID/Ti)
q2 = Kp*Td/Ts_PID

%step(g1)
%grid on
%hold on
%step(g1_z)