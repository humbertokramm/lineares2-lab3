from control import TransferFunction as tf,feedback, zero
from numpy import log, pi, prod
import matplotlib.pyplot as plt
import csv
from scipy.signal import lti, step
from math import e

#Pontos de resolução
dots = 5000

def coef (raizes):
	grau = len(raizes)
	coeficientes=[]
	
	coeficientes.append(1)
	coeficientes.append(sum(abs(raizes.real)))
	if(grau == 3): coeficientes.append(abs((raizes[0]*raizes[1]).real) + abs((raizes[0]*raizes[2]).real) +abs((raizes[2]*raizes[1]).real))
	coeficientes.append(prod(abs(raizes)))
	return coeficientes

#Parâmetros do FT experimental
wn1 = 40.21
wn2 = 207.27
k = 6417.43
TP = 0.0036
MP = 0.13

#Método de Alocação de Pólos
b0 = k
a1 = wn1+wn2
a0 = wn1*wn2
g = tf([b0],[1,a1,a0])
print(g)
#g = zpk([],[-wn1 -wn2],b0) %matlab

ksi = ( ((log(MP)/pi)**2) / (1+(log(MP)/pi)**2) )**(1/2)
wn = pi/(TP*((1-ksi**2)**(1/2)))
#print('wn = ',wn)
Z1 = 5*wn
#print('Z1 = ',Z1)

Q = tf([1,Z1],[1])*tf([1,2*ksi*wn,wn**2],[1])
[_,q2,q1,q0] = coef(zero(Q))

#print('q0 = ',q0)
#print('q1 = ',q1)
#print('q2 = ',q2)

C0 = q0/b0
C1 = (q1-a0)/b0
C2 = (q2-a1)/b0

Kp = C1
Td = C2/Kp
Ti = Kp/C0

Ci = tf([1/Ti],[1,0])
Cd = tf([Td,0],[1])

PI = (1+Ci)
print('PI=',PI)

sys = feedback(PI*Kp*g/(1+Kp*g*Cd))
sys3 = feedback((PI+Cd)*Kp*g)
print("sys= ",sys)
#Aplica o degrau
sys2 = sys.returnScipySignalLTI()[0][0]
t,y = step(sys2,N = dots)

sys4 = sys3.returnScipySignalLTI()[0][0]
t4,y4 = step(sys4,N = dots)

#Corta o fnal do y
for i in range (len(t)):
    if(t[i]>15e-3): break

y = y[:i]
t = t[:i]

#Faz um degrau só para exibir gráficamente
Tstep = [-t.max()/50,0,1e-15,t.max()]
Ystep = [0,0,1,1]


#prepara algumas informações da legenda
Pico = max(y)
#print("Pico = ",Pico)
Final = 1
#print("Estabiliza em  = ",y[-1])
SobreSinal_c = 100*(Pico-Final)/Final
#print("SobreSinal_c = ",SobreSinal_c,"%")
Erro = (1-y[-1])*100
#print("Erro = ",Erro,"%")

#prepara algumas informações da legenda
PicoPID = max(y4)
#print("Pico = ",Pico)
FinalPID = 1
#print("Estabiliza em  = ",y[-1])
SobreSinal_cPID = 100*(PicoPID-FinalPID)/FinalPID
#print("SobreSinal_c = ",SobreSinal_c,"%")
ErroPID = (1-y4[-1])*100
#print("Erro = ",Erro,"%")



titulo = 'Método de Alocação de Pólos'
subtitulo = '\nStep Response - (Kp='
subtitulo += str(round(Kp,3))
subtitulo += ' Ti='
subtitulo += str(round(Ti*1e3,3))
subtitulo += 'm Td='
subtitulo += str(round(Td*1e3,3))
subtitulo += 'm)'

#Monta o Gráfico
plt.title(titulo+subtitulo)                  # Titulo
plt.plot(Tstep,Ystep,label='Step')
#plt.plot([t[0],t[-1]],[Pico,Pico],label='Sobressinal: MP = '+str(round(SobreSinal_c,3))+'%' )
plt.plot(t4,y4,label='(PID) Erro = '+str(round(ErroPID,3))+'% ; MP = '+str(round(SobreSinal_cPID,3))+'%')                # Titulo
plt.plot(t,y,label='(PI-D) Erro = '+str(round(Erro,3))+'% ; MP = '+str(round(SobreSinal_c,3))+'%' )                # Titulo

plt.ylabel('gain')        # Plota o label y
plt.xlabel('time [s]')            # Plota o label x
plt.grid(which='both', axis='both')     # Gride para frequências intermediárias
plt.grid(True)                          # Mostra o Grid
plt.margins(0, 0.1)                     # Deixa uma margem
plt.legend()
plt.savefig(titulo+' em PI-D.png')

plt.show()