from control import TransferFunction as tf,feedback, zero,sample_system as c2d,bode_plot
from numpy import log, pi, prod
import matplotlib.pyplot as plt
import csv
from scipy.signal import lti, step,bode
from math import e

wn1 = 0.5
wn2 = 0.1
k = 10

sys = k*tf([1,wn1],[1,wn2])
print(sys)

Ts= 1
sys_z = c2d(sys,Ts,'zoh')

print(sys_z)

#slide 11, da aula 15
#dado um controlador...
wn1 = 4.41
wn2 = 18.4
k = 41.71

sys = k*tf([1,wn1],[1,wn2])
print(sys)

Ts= 1/200
#bode(sys.returnScipySignalLTI()[0][0])
bode_plot(sys,dB = True,omega_limits = [0.1,1000])

c_z = c2d(sys,1/200,'zoh')